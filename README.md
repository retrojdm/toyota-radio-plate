# Toyota Shaft-Style Radio Face Plate

![](./screenshots/photoshop.png)

3D-printable faceplates to "hide the crimes" when your original dash has been molested.

If you can't be bothered with all this "making", you can just order one from [my Shapeways shop](https://www.shapeways.com/shops/retrojdm).

# Source Files

Otherwise, I've provided two variants for the first-gen Celica ready to go: early (A21/A22//A25/A27) and late (A28/A29/A35). The only real difference is the power symbol in the RA28 labels.

I had a TA22 radio in [my RA28](http://www.retrojdm.com/GalleryView.asp?GalleryID=1) for a while before buidling my own DIY [Arduino Car Stereo](http://www.retrojdm.com/ArduinoCarStereo.asp), and as far as I know the shaft-spacing and nose hole size is the same.

Annoyingly, the shafts aren't vertically centered with the nose like they are on some other old Toyotas (like the RN41 Hilux).

Note: The font used is [Inter](https://fonts.google.com/specimen/Inter) Bold, which is a free and open-source alternative to Helvetica on Google Fonts.

### STL Files

The STL files are the ones you probably want:

- [toyota-radio-plate-early.stl](./toyota-radio-plate-early.stl)
- [toyota-radio-plate-late.stl](./toyota-radio-plate-late.stl)

### FreeCAD Files

You can modify the FreeCAD files to fit any other shaft-style radio or car.

- [toyota-radio-plate-early.FCStd](./toyota-radio-plate-early.FCStd)
- [toyota-radio-plate-late.FCStd](./toyota-radio-plate-late.FCStd)

![](./screenshots/freecad-late.png)

I've made all of the dimensions editable via the "parameters" spreadsheet in the FreeCAD projects. That should cover most cases. If you want to get fancier than that you're on your own ;)

![](./screenshots/freecad-late-parameters.png)

Also, if you modify the FreeCAD project and want to export your changes to an STL file for printing, you'll have to use the "Mesh" workbench. Again, YouTube videos will be your best learning resource for this stuff.

# Important Dimensions

If you're curious, here are the dimensions used:

| Dimension         |  Length | Notes                                                    |
| :---------------- | ------: | :------------------------------------------------------- |
| Width             |  165 mm |                                                          |
| Height            |   52 mm |                                                          |
| Depth             |    3 mm | Thickness of plate is actually 2mm because of the cavity |
| Plate Rear Cavity |    1 mm | To clear the original embossed labels                    |
| Nose Width        | 85.5 mm | Nose = big rectangular center hole                       |
| Nose Height       |   32 mm |                                                          |
| Text/Rim Depth    | 0.75 mm | emboss protrusion                                        |
| Shaft Spacing     |  123 mm | center-to-center                                         |
